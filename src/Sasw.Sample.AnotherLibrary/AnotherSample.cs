﻿using System;

namespace Sasw.Sample.AnotherLibrary
{
    public class AnotherSample
    {
        public string Test()
        {
            return "I am another sample";
        }
    }
}